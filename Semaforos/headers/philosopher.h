#pragma once

#include <pthread.h>

typedef enum {
	THINKING 	= 0,
	HUNGRY		= 1,
	EATING		= 2
} PhilosopherState;

typedef enum {
	LEFT	= 0,
	RIGHT	= 1
} SidePreference;

typedef struct {
	int left_fork;
	int right_fork;
	SidePreference preference;
	PhilosopherState state;

	pthread_t thread;
} Philosopher;
