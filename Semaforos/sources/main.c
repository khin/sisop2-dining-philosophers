#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <time.h>
#include "philosopher.h"

#define N 5
#define MAX_WAIT 10

Philosopher philosophers[N];
sem_t forks[N];
sem_t printing;

void *Philosophize(void* index);
void PrintPhilosophersStates();

char StateToChar(PhilosopherState state);
int GetPreferredForkIndex(int p_index);
int GetOtherForkIndex(int p_index);

int main(int argc, char* argv[]){
	srand(time(NULL));

	printf("N = %d\n", N);

	sem_init(&printing, 0, 1);
	for (int i = 0; i < N; i++){
		sem_init(&(forks[i]), 0, 1);
		printf("T ");
	}

	printf("\n");
	for (int i = 0; i < N; i++){
		philosophers[i].left_fork	= i;
		philosophers[i].right_fork	= (i + 1) % N;
		philosophers[i].state		= THINKING;

		if (i == 0)	philosophers[i].preference = LEFT;
		else		philosophers[i].preference = RIGHT;

		if (pthread_create(&(philosophers[i].thread), NULL, Philosophize, &i) != 0)
			printf("Error creating thread %d\n", i);
		else{
			// Prevents changes to the value of 'i' before the thread is created
			sleep(1);
		}
	}
	
	pthread_exit(0);
}

void *Philosophize(void* arg){
	int index = *(int*)arg;
	Philosopher *p = &philosophers[index];

	sleep(N - index);
	while(1){
		switch (p->state){
			case THINKING:
				sleep((rand() % MAX_WAIT) + 1);
				p->state = HUNGRY;

				PrintPhilosophersStates();
				break;
			case EATING:
				sleep((rand() % MAX_WAIT) + 1);
				p->state = THINKING;
				PrintPhilosophersStates();

				sem_post(&(forks[GetOtherForkIndex(index)]));
				sem_post(&(forks[GetPreferredForkIndex(index)]));
				break;
			case HUNGRY:
				sem_wait(&(forks[GetPreferredForkIndex(index)]));
				sem_wait(&(forks[GetOtherForkIndex(index)]));

				p->state = EATING;
				PrintPhilosophersStates();
				break;
		}
	}

	pthread_exit(0);
}

int GetPreferredForkIndex(int p_index){
	if (philosophers[p_index].preference == LEFT){
		return philosophers[p_index].left_fork;
	}

	return philosophers[p_index].right_fork;
}

int GetOtherForkIndex(int p_index){
	if (philosophers[p_index].preference == LEFT){
		return philosophers[p_index].right_fork;
	}

	return philosophers[p_index].left_fork;
}

char StateToChar(PhilosopherState state){
	char result;

	switch (state){
		case THINKING:	result = 'T'; break;
		case HUNGRY:	result = 'H'; break;
		case EATING:	result = 'E'; break;
	}

	return result;
}

void PrintPhilosophersStates(){
	sem_wait(&printing);

	for (int i = 0; i < N; i++)
		printf("%c ", StateToChar(philosophers[i].state));
	printf("\n");

	sem_post(&printing);
}
