#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include <unistd.h>
#include <time.h>
#include "philosopher.h"

#define N 5
#define MAX_WAIT 10

Philosopher philosophers[N];

int fork_state[N];	// 1 if fork is in use, 0 otherwise

pthread_mutex_t forks[N];
pthread_cond_t fork_queue[N];
pthread_mutex_t printing;

void *Philosophize(void* index);
void PrintPhilosophersStates();

char StateToChar(PhilosopherState state);
int GetPreferredForkIndex(int p_index);
int GetOtherForkIndex(int p_index);

int main(int argc, char* argv[]){
	srand(time(NULL));

	printf("N = %d\n", N);

	pthread_mutex_init(&printing, NULL);
	for (int i = 0; i < N; i++){
		pthread_mutex_init(&(forks[i]), NULL);
		pthread_cond_init(&(fork_queue[i]), NULL);
		fork_state[i] = 0;

		printf("T ");
	}

	printf("\n");
	for (int i = 0; i < N; i++){
		philosophers[i].left_fork	= i;
		philosophers[i].right_fork	= (i + 1) % N;
		philosophers[i].state		= THINKING;

		if (i == 0)	philosophers[i].preference = LEFT;
		else		philosophers[i].preference = RIGHT;

		//printf("Philosopher %d, left fork %d, right fork %d", i, i, (i + 1) % N);
		//if (philosophers[i].preference == LEFT)	printf(", prefers left fork\n");
		//else 									printf(", prefers right fork\n");

		if (pthread_create(&(philosophers[i].thread), NULL, Philosophize, &i) != 0)
			printf("Error creating thread %d\n", i);
		else{
			// Prevents changes to the value of 'i' before the thread is created
			sleep(1);
		}
	}

	pthread_exit(0);
}

void *Philosophize(void* arg){
	int index = *(int*)arg;
	Philosopher *p = &philosophers[index];

	int preferred_fork	= GetPreferredForkIndex(index);
	int other_fork		= GetOtherForkIndex(index);

	sleep(N - index);
	while(1){
		switch (p->state){
			case THINKING:
				sleep((rand() % MAX_WAIT) + 1);
				p->state = HUNGRY;

				PrintPhilosophersStates();
				break;
			case EATING:
				sleep((rand() % MAX_WAIT) + 1);
				p->state = THINKING;
				PrintPhilosophersStates();

				fork_state[other_fork] = 0;
				pthread_mutex_unlock(&(forks[other_fork]));
				pthread_cond_signal(&(fork_queue[other_fork]));

				fork_state[preferred_fork] = 0;
				pthread_mutex_unlock(&(forks[preferred_fork]));
				pthread_cond_signal(&(fork_queue[preferred_fork]));
				break;
			case HUNGRY:
				pthread_mutex_lock(&(forks[preferred_fork]));
				while (fork_state[preferred_fork] != 0)
					pthread_cond_wait(&(fork_queue[preferred_fork]), &(forks[preferred_fork]));
				fork_state[preferred_fork] = 1;

				pthread_mutex_lock(&(forks[other_fork]));
				while (fork_state[other_fork] != 0)
					pthread_cond_wait(&(fork_queue[other_fork]), &(forks[other_fork]));
				fork_state[other_fork] = 1;

				p->state = EATING;
				PrintPhilosophersStates();
				break;
		}
	}

	pthread_exit(0);
}

int GetPreferredForkIndex(int p_index){
	if (philosophers[p_index].preference == LEFT){
		return philosophers[p_index].left_fork;
	}

	return philosophers[p_index].right_fork;
}

int GetOtherForkIndex(int p_index){
	if (philosophers[p_index].preference == LEFT){
		return philosophers[p_index].right_fork;
	}

	return philosophers[p_index].left_fork;
}

char StateToChar(PhilosopherState state){
	char result;

	switch (state){
		case THINKING:	result = 'T'; break;
		case HUNGRY:	result = 'H'; break;
		case EATING:	result = 'E'; break;
	}

	return result;
}

void PrintPhilosophersStates(){
	pthread_mutex_lock(&printing);

	for (int i = 0; i < N; i++)
		printf("%c ", StateToChar(philosophers[i].state));
	printf("\n");

	pthread_mutex_unlock(&printing);
}
